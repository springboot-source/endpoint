package com.hanggold.endpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-23 08:27
 * @since
 **/
@SpringBootApplication
public class EndpointApplication {

    public static void main(String[] args) {

        SpringApplication.run(EndpointApplication.class, args);

        System.out.println("启动成功.....");
    }

}
