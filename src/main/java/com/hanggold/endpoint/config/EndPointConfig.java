package com.hanggold.endpoint.config;

import com.hanggold.endpoint.controller.EndpointCustom;
import com.hanggold.endpoint.controller.JmxEndpointCustom;
import com.hanggold.endpoint.controller.WebEndpointCustom;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-23 08:31
 * @since
 **/
@Configuration
public class EndPointConfig {

    /**
     * 定义webEnpoint
     */
    @Bean
    public WebEndpointCustom webEndpointCustom() {
        WebEndpointCustom webEndpointCustom = new WebEndpointCustom();
        return webEndpointCustom;
    }

    /**
     * 定义jmxEndpoint
     */
    @Bean
    public JmxEndpointCustom jmxEndpointCustom() {
        JmxEndpointCustom jmxEndpointCustom = new JmxEndpointCustom();
        return jmxEndpointCustom;
    }

    /**
     * 定义基础的Endpoint
     *
     * @return
     */
    @Bean
    public EndpointCustom endpointCustom() {
        EndpointCustom endpointCustom = new EndpointCustom();
        return endpointCustom;
    }

}
