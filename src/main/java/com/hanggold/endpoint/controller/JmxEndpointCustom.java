package com.hanggold.endpoint.controller;

import org.springframework.boot.actuate.endpoint.annotation.DeleteOperation;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.boot.actuate.endpoint.jmx.annotation.JmxEndpoint;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-23 09:43
 * @since
 **/
@JmxEndpoint(id = "jmxendpointCustom")
public class JmxEndpointCustom {

    @ReadOperation
    public String jmxRead(String content) {
        return "jmx读取的内容： " + content;
    }

    @WriteOperation
    public String jmxWrite(String content) {
        return "jmx写内容: " + content;
    }

    @DeleteOperation
    public String jmxDelete(String content) {
        return "jmx删除内容: " + content;
    }




}
