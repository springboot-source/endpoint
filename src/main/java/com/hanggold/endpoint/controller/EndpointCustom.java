package com.hanggold.endpoint.controller;

import org.springframework.boot.actuate.endpoint.annotation.DeleteOperation;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-23 09:43
 * @since
 **/
@Endpoint(id = "endpointCustom")
public class EndpointCustom {

    @ReadOperation
    public String endpointCustomRead(String content) {
        return "你请求的内容: " + content;
    }

    @WriteOperation
    public String endpointCustomWrite(String content) {
        return "你写的内容: " + content;
    }

    @DeleteOperation
    public String endpointCustomDelete(String content) {
        return "你删除的内容: " + content;
    }

}
