package com.hanggold.endpoint.controller;

import org.springframework.boot.actuate.endpoint.annotation.DeleteOperation;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.boot.actuate.endpoint.web.annotation.WebEndpoint;

/**
 * @author <a href="mailto:sunmch@163.com">smc</a>
 * @date 2019-07-23 08:31
 * @since
 **/
@WebEndpoint(id = "webendpointCustom")
public class WebEndpointCustom {

    @ReadOperation
    public String webRead(String content) {
        return "webEndpoint 读取内容: " + content;
    }

    @WriteOperation
    public String webWrite(String content) {
        return "webEndpoint 写内容: " + content;
    }

    @DeleteOperation
    public String webDelete(String content) {
        return "webEndpoint 删除内容: " + content;
    }

}
