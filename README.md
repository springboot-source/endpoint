#  springboot2.0+中自定义Endpoint

楼主搜索整个某度的技术论坛，说到endpoint全部是千篇一律，而且都是停留在springboot1.0+的基础上介绍，而且全部是一样的文章，一点新意也没有，springboot2.0已经出来有一段时间了，我们有必要知道在springboot2.0+中怎么自定义一个Endpoint。闲话不说，直蹦主题。

## 操作描述

### 先来看看Endpoint中核心的几个注解

* 注解Endpoint

  ~~~java
  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  public @interface Endpoint {
  
  	/**
  	 * The id of the endpoint (must follow {@link EndpointId} rules).
  	 * @return the id
  	 * @see EndpointId
  	 */
  	String id() default "";
  
  	/**
  	 * If the endpoint should be enabled or disabled by default.
  	 * @return {@code true} if the endpoint is enabled by default
  	 */
  	boolean enableByDefault() default true;
  
  }
  ~~~

* 注解WebEndpoint

  ~~~java
   @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  @Endpoint
  @FilteredEndpoint(WebEndpointFilter.class)
  public @interface WebEndpoint {
  
  	/**
  	 * The id of the endpoint.
  	 * @return the id
  	 */
  	@AliasFor(annotation = Endpoint.class)
  	String id();
  
  	/**
  	 * If the endpoint should be enabled or disabled by default.
  	 * @return {@code true} if the endpoint is enabled by default
  	 */
  	@AliasFor(annotation = Endpoint.class)
  	boolean enableByDefault() default true;
  
  }
  ~~~

* 注解JmxEndpoint

  ~~~java
  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  @Endpoint
  @FilteredEndpoint(JmxEndpointFilter.class)
  public @interface JmxEndpoint {
  
  	/**
  	 * The id of the endpoint.
  	 * @return the id
  	 */
  	@AliasFor(annotation = Endpoint.class)
  	String id() default "";
  
  	/**
  	 * If the endpoint should be enabled or disabled by default.
  	 * @return {@code true} if the endpoint is enabled by default
  	 */
  	@AliasFor(annotation = Endpoint.class)
  	boolean enableByDefault() default true;
  }
  
  ~~~

  翻看上面的几个注解很清楚的知道Endpoint 是基础注解而且这些注解只能标注在**类上面**,类比Endpoint相当于Spring中的`Component`。`WebEndpoint`和 `JmxEndpoint`相当于`Endpoint`的派生类。下面通过一个表格介绍这几个注解的区别



| 注解         | 描述                                                         | 类比                                                |
| ------------ | ------------------------------------------------------------ | --------------------------------------------------- |
| @Endpoint    | 该注解的类可以通过http查看也可以通过jmx查看，他是在两个地方注册 | 相当于springmvc中的RestController和JMX中MBean的集合 |
| @JmxEndpoint | 该注解的类开放的是JMX接口                                    | 相当于JMX中的MBean                                  |
| @WebEndpoint | 该注解的类开饭的是http接口                                   | 相当于mvc当中的RestController                       |



接下来说下标注方法的几个注解

* 读操作ReadOperation

  ~~~java
  @Target(ElementType.METHOD)
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  public @interface ReadOperation {
  
  	/**
  	 * The media type of the result of the operation.
  	 * @return the media type
  	 */
  	String[] produces() default {};
  }
  ~~~

* 写操作 WriteOperation

  ~~~java
  @Target(ElementType.METHOD)
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  public @interface WriteOperation {
  
  	/**
  	 * The media type of the result of the operation.
  	 * @return the media type
  	 */
  	String[] produces() default {};
  
  }
  
  ~~~

* 删除操作DeleteOperation

  ~~~
  @Target(ElementType.METHOD)
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  public @interface DeleteOperation {
  
  	/**
  	 * The media type of the result of the operation.
  	 * @return the media type
  	 */
  	String[] produces() default {};
  
  }
  ~~~

  从注解中可以看到，这些标注的是方法注解顾名思义这些是关于操作方面的注解，同样下面通过一个表格来解释一下。

  | 描述              | 描述             | 类比                        |
  | ----------------- | ---------------- | --------------------------- |
  | @WriteOperation   | http-POST请求    | 相当于mvc中的@PostMapping   |
  | @ReadOperation    | http-  GET请求   | 相当于mvc中的@GetMapping    |
  | @DeleteOpretation | http- DELETE请求 | 相当于mvc中的@DeleteMapping |

### 代码实战

我们知道spring中的注解扫描是依靠`Component`注解，但是我们上面的注解`Endpoint`并不是注解 `Component`的派生。所以如果想要使用Endpoint类我们可以通过注解`Bean` 实例化。下面通过代码在操作`Endpoint`,`WebEndpoint` , `JmxEndpoint`注解操作。

* 配置config配置类

  ~~~java
  @Configuration
  public class EndPointConfig {
  
      /**
       * 定义webEnpoint
       */
      @Bean
      public WebEndpointCustom webEndpointCustom() {
          WebEndpointCustom webEndpointCustom = new WebEndpointCustom();
          return webEndpointCustom;
      }
  
      /**
       * 定义jmxEndpoint
       */
      @Bean
      public JmxEndpointCustom jmxEndpointCustom() {
          JmxEndpointCustom jmxEndpointCustom = new JmxEndpointCustom();
          return jmxEndpointCustom;
      }
  
      /**
       * 定义基础的Endpoint
       * @return
       */
      @Bean
      public EndpointCustom endpointCustom() {
          EndpointCustom endpointCustom = new EndpointCustom();
          return endpointCustom;
      }
  
  }
  ~~~

* 编写基础Endpoint

  ~~~java
  @Endpoint(id = "endpointCustom")
  public class EndpointCustom {
  
      @ReadOperation
      public String endpointCustomRead(String content) {
          return "你请求的内容: " + content;
      }
  
      @WriteOperation
      public String endpointCustomWrite(String content) {
          return "你写的内容: " + content;
      }
  
      @DeleteOperation
      public String endpointCustomDelete(String content) {
          return "你删除的内容: " + content;
      }
  
  }
  ~~~

  * 测试URL地址

    > curl  -X GET  http://localhost:8080/actuator/endpointCustom?content=endpointGet
    >
    > 你请求的内容: endpointGet
    >
    > curl -X POST http://localhost:8080/actuator/endpointCustom?content=endpointPost
    >
    > 你写的内容: endpointPost
    >
    > curl -X DELETE http://localhost:8080/actuator/endpointCustom?content=endpointDELETE
    >
    > 你删除的内容：endpointDELETE

  * 我们再来看看JMX

    ![第一张](https://images.gitee.com/uploads/images/2019/0723/150745_b34b892b_662451.png)

   

  

  `WebEndpoint` `JmxEndpoint` 和`Endpoint`使用相同，在此就不在赘述，只是`WebEndpoint` 只会通过Http访问，而`JmxEndpoint`是通过JMX查看而已。

​        

​       完整的代码如下：

​      https://gitee.com/springboot-source/endpoint.git





  

























